from map import Map
from pseudorandom import PseudoRandom
import pygame
from block import Wall
class DungeonCreator(Map):
    roomsGroup = pygame.sprite.Group()
    allRooms = []
    roomX = 0
    roomY = 0
    roomIds = []
    coords = []
    metaRoomCols = 0
    metaRoomRows = 0
    level = 0
    cycles = 0
    pseudoRand = 0
    corridors = pygame.sprite.Group()
    floors = pygame.sprite.Group()
    metaRoomWidth = 0
    metaRoomHeight = 0
    c = pygame.sprite.Group()
   
    def __init__(self, level, cycles, cols, rows, height, width):
        self.pseudoRand = PseudoRandom(level)
        self.cycles = cycles
        self.metaRoomCols = cols
        self.metaRoomRows = rows
        self.metaRoomWidth = width
        self.metaRoomHeight = height
        self.randomRoom()

    def randomRoom(self):
        roomCols = 0
        roomCols2 = 1
        roomRows = 0
        roomRows2 = 1
        roomId = []

        for l in range(1, self.cycles):

            roomWidth = self.pseudoRand.randint(8, self.metaRoomWidth/self.metaRoomCols - 2)
            roomHeight = self.pseudoRand.randint(8, self.metaRoomHeight/self.metaRoomCols - 2)

            if roomCols == self.metaRoomCols:
                roomRows += 1
                roomRows2 += 1
                roomCols = 0
                roomCols2 = 1

            roomX = self.pseudoRand.randint(self.metaRoomWidth/self.metaRoomCols * roomCols + 1, self.metaRoomWidth/self.metaRoomCols * roomCols2 - roomWidth - 1)
            roomY = self.pseudoRand.randint(self.metaRoomHeight/self.metaRoomRows * roomRows + 1, self.metaRoomHeight/self.metaRoomRows * roomRows2 - roomHeight - 1)

            roomCols += 1
            roomCols2 += 1
            if self.pseudoRand.randint(0, 10) > 6:
                self.roomIds.append(0)
                self.coords.append((0,0))
                self.allRooms.append(0)
                continue
            row = []

            for i in range(roomHeight):
                column = []
                for j in range(roomWidth):
                    column.append("F")
                row.append(column)

            for i in range(0, roomWidth, 1):
                row[0][i] = "WL"

            for i in range(0, roomWidth, 1):
                row[roomHeight - 1][i] = "WR"

            for i in range(1, roomHeight, 1):
                row[i][0] = "WT"

            for i in range(0, roomHeight, 1):
                row[i][roomWidth - 1] = "WB"

            row[0][0] = "CTL"
            row[0][roomWidth-1] = "CBL"
            row[roomHeight-1][0] = "CTR"
            row[roomHeight-1][roomWidth-1] = "CBR"


            self.roomIds.append(l)
            self.allRooms.append(row)
            self.coords.append((roomX,roomY))

            self.groupRooms(roomX, roomY, row)
            self.roomsGroup.add(self.floors)
            self.getRoomCenter()

    def groupRooms(self, x, y, row):
        for i in range(len(row)):
            for j in range(len(row[i])):
                if row[i][j] == "F":
                    floor = Wall("carpet.jpg")
                if row[i][j] == "WL":
                    floor = Wall("wall_left.jpg")
                if row[i][j] == "WR":
                    floor = Wall("wall_right.jpg")
                if row[i][j] == "WT":
                    floor = Wall("wall_top.jpg")
                if row[i][j] == "WB":
                    floor = Wall("wall_bottom.jpg")
                if row[i][j] == "CTL":
                    floor = Wall("wall_ctl.jpg")
                if row[i][j] == "CTR":
                    floor = Wall("wall_ctr.jpg")
                if row[i][j] == "CBR":
                    floor = Wall("wall_cbr.jpg")
                if row[i][j] == "CBL":
                    floor = Wall("wall_cbl.jpg")
                floor.rect.x = x * 50 + i * 50
                floor.rect.y = y * 50 + j * 50
                self.floors.add(floor)

    def generateCorridor(self, roomCenters):
        center = roomCenters[0]
        other = roomCenters[1]

        yC = center[1]
        while yC != other[1]:
            if center[1] > other[1]:
                yC -= 1
            else:
                yC += 1
            floor = Wall("carpet.jpg")
            floor.rect.x = center[0] * 50
            floor.rect.y = yC * 50
            self.corridors.add(floor)

        xC = center[0]
        while xC != other[0]:
            if center[0] > other[0]:
                xC -= 1
            else:
                xC += 1
            floor = Wall("carpet.jpg")
            floor.rect.x = xC * 50
            floor.rect.y = yC * 50
            self.corridors.add(floor)
        return self.corridors

    def getRoomCenter(self):
            rooms =  self.allRooms
            metaRoomCols = self.metaRoomCols
            metaRoomRows = self.metaRoomRows
            coords = self.coords
            roomID = self.roomIds
            for i in range(len(rooms)):
                if roomID[i] == 0:
                    continue
                width = len(rooms[i][0])
                height = len(rooms[i])

                roomX = coords[i][0]
                roomY = coords[i][1]

                center1 = (roomX + width/2, roomY + height/2)
                for j in range(1, (metaRoomCols - (i%metaRoomCols))):
                    ri = i + j
                    if ri > len(roomID) - 1:
                        break
                    if roomID[ri] != 0:
                        centerRight = (coords[ri][0] + len(rooms[ri][0])/2, coords[ri][1] + len(rooms[ri])/2)
                        centers = (center1, centerRight)
                        self.c.add(self.generateCorridor(centers))
                        break
                for j in range(1, metaRoomRows - (i/metaRoomCols)):
                    ri = i + j*metaRoomCols
                    if ri >=len(roomID):
                        break
                    if roomID[ri] != 0:
                        centerBottom = (coords[ri][0] + len(rooms[ri][0])/2, coords[ri][1] + len(rooms[ri])/2)
                        centers = (center1, centerBottom)
                        self.c.add(self.generateCorridor(centers))
                        break

    def drawRooms(self,screen):
        self.draw(self.floors, screen)
        self.draw(self.corridors, screen)
