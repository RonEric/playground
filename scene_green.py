import pygame
import math
import scene
from character import character
import scene_overworld

class SceneGreen(scene.scene):
    def __init__(self, game):

        self.background_color = (10, 100, 10)
        self.game = game

    def loop(self, screen):
        screen.fill(self.background_color)
        pygame.display.flip()

        if (pygame.key.get_pressed())[pygame.K_ESCAPE]:
            self.game.set_current_scene(scene_overworld.SceneOverworld(self.game))
