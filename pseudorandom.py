import random
import md5
import struct

class PseudoRandom():

    seed = []

    def __init__(self, level):

        if len(self.seed) == 0:
            random.seed(random.random())
            self.seed.append(random.randint(0, 2**16 - 1))
        seed = self.seed[0]

        m = md5.new()
        m.update(struct.pack("=H", seed))
        m.update(" ")
        m.update(str(level))
        self.hash = m.digest()
        self.hashpos = 0

    def update_hash(self):
        m = md5.new()
        m.update(self.hash)
        self.hash = m.digest()
        self.hashpos = 0

    def get_hash_part(self, len):
        if self.hashpos + len > 15:
            self.update_hash()
        part = self.hash[self.hashpos:self.hashpos + len]
        self.hashpos += len
        return part

    def random(self):
        maxval = 65536 + 1.0
        val, = struct.unpack("=H", self.get_hash_part(2))
        return val / maxval

    def randint(self, a, b):
        return a + int(self.random() * (b - a + 1))

if __name__ == "__main__":

    pg = PseudoRandom(3)
    for i in range(20):
        print pg.random()

    dist = [0] * 7
    for i in range(1000000):
        val = pg.randint(0, 6)
        dist[val] = dist[val] + 1

    print dist
