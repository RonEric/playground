
import pygame
import math
import scene
from block import Wall
from map_creator import MapCreator
from DungeonCreator import DungeonCreator
from character import Character


class SceneOverworld(scene.scene,):
    def __init__(self, game):
        drawn = False
        self.drawn = drawn
        self.dungeonCreator = DungeonCreator(1, 30, 3, 3, 2000/50, 2000/50)
        p = Wall("character.png")
        self.player = Character(p)

        self.background_color = (0, 0, 0)

    def loop(self, screen):
        screen.fill(self.background_color)
        self.dungeonCreator.drawRooms(screen)
        self.player.drawCharacter(screen)
        pygame.display.flip()
