import pygame

class Drawable():
    def __init__(self):
        pass

    def draw(self, group, screen):
        group2 = pygame.sprite.Group()
        for block in group:
            key_list = pygame.key.get_pressed()
            if key_list[pygame.K_UP]:
                block.rect.y += 50
            if key_list[pygame.K_DOWN]:
                block.rect.y -= 50
            if key_list[pygame.K_LEFT]:
                block.rect.x += 50
            if key_list[pygame.K_RIGHT]:
                block.rect.x -= 50
            group.add(block)
            if (block.rect.x >= 0 and block.rect.x <= 1000) and (block.rect.y >= 0 and block.rect.y <= 1000):
                group2.add(block)
        group.draw(screen)
