from block import Wall
import pygame
from pseudorandom import PseudoRandom
import random
class MapCreator():
    rooms = []

    def __init__(self):
        random.seed()


    def createDungeon(self):
        walls = pygame.sprite.Group()
        self.walls = walls

        for i in range(0, 800, 50):
            for j in range(0, 600, 50):
                wall = Wall("wall.jpg")
                self.wall = wall
                self.wall.rect.x = i
                self.wall.rect.y = j
                self.walls.add(self.wall)


        return self.walls

    def drawDungeon(self, tiles, screen):
        tiles.draw(screen)

    def randomLabyrinth(self):
        row = []

        for j in range(14):
            column = []
            for i in range(10):
                column.append(0)
            row.append(column)

        xStart = random.randint(0, 13)
        yStart = random.randint(0, 9)

        row[xStart][yStart] = 1

        x = xStart
        y = yStart
        i = 0

        j = 0

        dirNew = None
        dirOld = None

        while j < 200:
            while dirNew == dirOld:
                dir = random.randint(1, 4)
                dirNew = dir

            else:
                j += 1
                pathLength = random.randint(1, 10)

                if dir == 1:
                    x = x - 1
                    while i < pathLength:
                        if (x > 0 and x < 13) and (y > 0 and y < 9):
                            if (row[x + 1][y - 1] == 0 or row[x][y - 1] == 0) and (row[x + 1][y + 1] == 0 or row[x][y + 1] == 0):
                                if x > 0:
                                    x -= 1
                                    row[x][y] = 1
                                else:
                                    x = x + 1
                                    i = pathLength
                            else:
                                x = x + 1
                                i = pathLength
                        i += 1


                if dir == 2:
                    y = y - 1
                    while i < pathLength:
                        if x > 0 and x < 13 and y > 0 and y < 9:
                            if (row[x - 1][y - 1] == 0 or row[x - 1][y] == 0) and (row[x + 1][y - 1] == 0 or row[x + 1][y] == 0):

                                if y > 0:
                                    y -= 1
                                    row[x][y] = 1

                                else:
                                    y = y + 1
                                    i = pathLength


                        else:
                            y = y + 1
                            i = pathLength

                        i += 1


                if dir == 3:
                    x = x + 1
                    while i < pathLength:
                        if x > 0 and x < 13 and y > 0 and y < 9:
                            if (row[x - 1][y - 1] == 0 or row[x][y - 1] == 0) and (row[x - 1][y + 1] == 0 or row[x][y + 1] == 0):
                                if x <= 14:
                                    x += 1
                                    row[x][y] = 1
                                else:
                                    x = x - 1
                                    i = pathLength
                        else:
                            x = x - 1
                            i = pathLength

                        i += 1


                if dir == 4:
                    y = y + 1
                    while i < pathLength:
                        if x > 0 and x < 13 and y > 0 and y < 9:
                            if (row[x - 1][y + 1] == 0 or row[x - 1][y] == 0) and (row[x + 1][y + 1] == 0 or row[x + 1][y] == 0):

                                if y <= 10:
                                    y += 1
                                    row[x][y] = 1
                                else:
                                    y = y - 1
                                    i = pathLength

                        else:
                            y = y - 1
                            i = pathLength
                        i += 1
                dirOld = dirNew
                dirNew = random.randint(1, 4)
                dir = dirNew
                i = 0

        return row

    def drawRandomLabyringth(self, row, screen):
        floors = pygame.sprite.Group()
        self.floors = floors
        for i in range(14):
            for j in range(10):
                if row[i][j] == 1:
                    floor = Wall("carpet.jpg")
                    self.floor = floor
                    self.floor.rect.x = i * 50
                    self.floor.rect.y = j * 50
                    self.floors.add(self.floor)
                    self.floors.draw(screen)

        return self.floors

    def randomRoom(self, screen, cycles, level):
        ps = PseudoRandom(level)
        metaRoomWidth = 5000/50
        metaRoomHeight = 5000/50
        metaRoomCols = 6
        metaRoomRows = 6
        roomCols = 0
        roomCols2 = 1
        roomRows = 0
        roomRows2 = 1
        rooms = pygame.sprite.Group()
        roomId = []
        roomIds = []
        all_rooms = []
        coords = []
        self.rooms = rooms
        for l in range(1, cycles):

            roomWidth = ps.randint(8, metaRoomWidth/metaRoomCols - 2)
            roomHeight = ps.randint(8, metaRoomHeight/metaRoomCols - 2)

            if roomCols == metaRoomCols:
                roomRows += 1
                roomRows2 += 1
                roomCols = 0
                roomCols2 = 1

            roomX = ps.randint(0 + metaRoomWidth/metaRoomCols * roomCols + 1, metaRoomWidth/metaRoomCols * roomCols2 - roomWidth - 1)
            roomY = ps.randint(0 + metaRoomHeight/metaRoomRows * roomRows + 1, metaRoomHeight/metaRoomRows * roomRows2 - roomHeight - 1)


            roomCols += 1
            roomCols2 += 1
            if ps.randint(0, 10) > 6:
                roomId.append(0)
                coords.append((0,0))
                all_rooms.append(0)
                continue
            row = []


            for i in range(roomHeight):
                column = []
                for j in range(roomWidth):
                    column.append("F")
                row.append(column)

            for i in range(0, roomWidth, 1):
                row[0][i] = "WL"

            for i in range(0, roomWidth, 1):
                row[roomHeight - 1][i] = "WR"

            for i in range(1, roomHeight, 1):
                row[i][0] = "WT"

            for i in range(0, roomHeight, 1):
                row[i][roomWidth - 1] = "WB"

            row[0][0] = "CTL"
            row[0][roomWidth-1] = "CBL"
            row[roomHeight-1][0] = "CTR"
            row[roomHeight-1][roomWidth-1] = "CBR"


            roomId.append(l)
            all_rooms.append(row)
            coords.append((roomX,roomY))
            data = (row, roomX, roomY, self.rooms, roomId, all_rooms, coords, metaRoomCols, metaRoomRows)

            self.data = data
            floors = self.groupRooms(self.data)
            self.floors = floors
            self.rooms.add(self.floors)
        return self.data


    def drawRooms(self,screen, data):
        self.rooms = data[3]
        self.rooms.draw(screen)

    def groupRooms(self, data):
        row = data[0]
        floors = pygame.sprite.Group()
        self.floors = floors
        for i in range(len(row)):
            for j in range(len(row[i])):
                if row[i][j] == "F":
                    floor = Wall("carpet.jpg")
                if row[i][j] == "WL":
                    floor = Wall("wall_left.jpg")
                if row[i][j] == "WR":
                    floor = Wall("wall_right.jpg")
                if row[i][j] == "WT":
                    floor = Wall("wall_top.jpg")
                if row[i][j] == "WB":
                    floor = Wall("wall_bottom.jpg")
                if row[i][j] == "CTL":
                    floor = Wall("wall_ctl.jpg")
                if row[i][j] == "CTR":
                    floor = Wall("wall_ctr.jpg")
                if row[i][j] == "CBR":
                    floor = Wall("wall_cbr.jpg")
                if row[i][j] == "CBL":
                    floor = Wall("wall_cbl.jpg")
                self.floor = floor
                self.floor.rect.x = data[1] * 50 + i * 50
                self.floor.rect.y = data[2] * 50 + j * 50
                self.floors.add(self.floor)
        return self.floors

    def generateCorridor(self, roomCenters, screen):
        center = roomCenters[0]
        other = roomCenters[1]
        floors= pygame.sprite.Group()
        floor = Wall("carpet.jpg")

        yC = center[1]
        while yC != other[1]:
            if center[1] > other[1]:
                yC -= 1
            else:
                yC += 1
            floor = Wall("carpet.jpg")
            floor.rect.x = center[0] * 50
            floor.rect.y = yC * 50
            floors.add(floor)

        xC = center[0]
        while xC != other[0]:
            if center[0] > other[0]:
                xC -= 1

            else:
                xC += 1
                print xC
            floor = Wall("carpet.jpg")
            floor.rect.x = xC * 50
            floor.rect.y = yC * 50
            floors.add(floor)
        return floors

    def drawCorridors(self, corridors, screen):
        self.corridors = corridors
        self.corridors.draw(screen)


    def getRoomCenter(self, data, screen):
            rooms =  data[5]
            metaRoomCols = data[7]
            metaRoomRows = data[8]
            coords = data[6]
            roomID = data[4]
            c = pygame.sprite.Group()
            for i in range(len(rooms)):
                if roomID[i] == 0:
                    continue
                width = len(rooms[i][0])
                height = len(rooms[i])

                roomX = coords[i][0]
                roomY = coords[i][1]

                center1 = (roomX + width/2, roomY + height/2)
                for j in range(1, (metaRoomCols - (i%metaRoomCols))):
                    ri = i + j
                    if ri > len(roomID) - 1:
                        break
                    if roomID[ri] != 0:
                        centerRight = (coords[ri][0] + len(rooms[ri][0])/2, coords[ri][1] + len(rooms[ri])/2)
                        centers = (center1, centerRight)
                        c.add(self.generateCorridor(centers, screen))
                        break
                for j in range(1, metaRoomRows - (i/metaRoomCols)):
                    ri = i + j*metaRoomCols
                    if ri >=len(roomID):
                        break
                    if roomID[ri] != 0:
                        centerBottom = (coords[ri][0] + len(rooms[ri][0])/2, coords[ri][1] + len(rooms[ri])/2)
                        centers = (center1, centerBottom)
                        c.add(self.generateCorridor(centers, screen))
                        break
            return c





    def roomList(self, data):
        self.rooms.append(data[0])
        return self.rooms
