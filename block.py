import pygame
image_resources = "sprites/"

class Wall(pygame.sprite.Sprite):
    def __init__(self,image):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(image_resources + image)
        self.rect = self.image.get_rect()

