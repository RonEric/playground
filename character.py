import pygame
from Drawable import Drawable

class Character(Drawable):


    def __init__(self, sprite):
        self.pos = (500-50,500-200)
        self.health = 100
        self.sprite = sprite
        self.sprite.rect.x = self.pos[0]
        self.sprite.rect.y = self.pos[1]

    def drawCharacter(self, screen):
        self.g = pygame.sprite.Group()
        self.g.add(self.sprite)
        self.g.draw(screen)
