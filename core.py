import pygame
import scene_overworld
import time

background_colour_overworld = (100, 10, 10)
background_colour_battle = (10, 10, 100)

class gameClass:

    def __init__(self):
        self.current_scene = None

    def set_current_scene(self, scene):
        del self.current_scene
        self.current_scene = scene

game = gameClass()
game.current_scene = scene_overworld.SceneOverworld(game)

(width, height) = (1000, 1000)
screen = pygame.display.set_mode((width, height))

running = True

millisStart = int(round(time.time() * 1000))

while running:
    millisNow = int(round(time.time() * 1000))
    #if millisNow - millisStart > 1:
    game.current_scene.loop(screen)
    millisStart = millisNow

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
